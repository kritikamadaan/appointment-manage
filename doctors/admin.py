from django.contrib import admin

from .models import Speciality, DoctorsProfile, Clinics,Procedures

# Register your models here.
class AdminClinics(admin.ModelAdmin):
    model = Clinics
    list_display = ('clinic_name', 'clinic_location', 'longitude', 'latitude', 'is_active')

class AdminDoctorsProfile(admin.ModelAdmin):
    model = DoctorsProfile
    list_display = (
        'first_name',
        'middle_name',
        'last_name',
        'gender',
        'dob',
        'email',
        'mobile_number',
        'speciality',
        'registration_number',
        'year_of_registration',
        'is_active'
    )


class AdminSpeciality(admin.ModelAdmin):
    model = Speciality
    list_display = ('speciality_name', 'is_active')

admin.site.register(Speciality, AdminSpeciality)
admin.site.register(DoctorsProfile, AdminDoctorsProfile)
admin.site.register(Clinics, AdminClinics)
admin.site.register(Procedures)
