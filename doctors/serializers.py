from rest_framework import serializers

from .models import (
    DoctorsProfile, 
    Speciality,
    Procedures, 
    Clinics
)

class SpecialitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Speciality
        fields = "__all__"


class DoctorsProfileListSerializer(serializers.ModelSerializer):
    speciality = SpecialitySerializer(read_only=True)
    class Meta:
        model = DoctorsProfile
        fields = "__all__"

class CreateDoctorProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = DoctorsProfile
        fields = "__all__"


class CreateAndListOfClinicsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Clinics
        fields = "__all__"

class ProceduresListSerializer(serializers.ModelSerializer):
    speciality = SpecialitySerializer(read_only=True)
    class Meta:
        model = Procedures
        fields ="__all__"

class CreateProceduresSerializer(serializers.ModelSerializer):
    class Meta:
        model = Procedures
        fields ="__all__"

class UpdateDoctorSerializer(serializers.ModelSerializer):
    class Meta:
        model = DoctorsProfile
        fields = "__all__"


class UpdateClinicSerializer(serializers.ModelSerializer):
    class Meta:
        model = Clinics
        fields = [
            "user",
            "id",
            "clinic_name",
            "clinic_location",
            "longitude",
            "latitude"
        ]


class UpdateProcedureSerializer(serializers.ModelSerializer):
    class Meta:
        model =Procedures
        fields = [
            "user",
            "id",
            "speciality",
            "procedures"
        ]

class UpdateSpecialitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Speciality
        fields = '__all__'
