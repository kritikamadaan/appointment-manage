from django.shortcuts import render

from rest_framework.permissions import IsAuthenticated, AllowAny, IsAdminUser
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.generics import (
    RetrieveUpdateAPIView,
)
from .serializers import (
    DoctorsProfileListSerializer, 
    CreateDoctorProfileSerializer,
    CreateAndListOfClinicsSerializer,
    SpecialitySerializer,
    ProceduresListSerializer,
    CreateProceduresSerializer,
    UpdateDoctorSerializer,
    UpdateClinicSerializer,
    UpdateProcedureSerializer,
    UpdateSpecialitySerializer
)
from hospital.adminpermissions import IsSuperUser

urlpatterns = [
    url(r'clinic/create-and-list/',views.CreateAndListOfClinicsApiViews.as_view()),
    url(r'speciality/',views.CreateAndListOfSpecialityApiViews.as_view()),
    url(r'speciality-details/(?P<id>[0-9]+)/$',views.SpecialityDetails.as_view()),
    url(r'procedures-details/(?P<id>[0-9]+)/$',views.ProceduresDetails.as_view()),
    url(r'clinic/details/(?P<id>[0-9]+)/$',views.ClinicDetails.as_view()),
    url(r'procedures/',views.CreateAndListOfPorocedures.as_view()),
    url(r'list/', views.DoctorsProfileListApiViews.as_view()),
    url(r'create-profile/',views.CreateDoctorProfileApiViews.as_view()),
    url(r'details/(?P<id>[0-9]+)/$', views.DoctorsProfileDetailsApiViews.as_view()),

    url(r'update-doctor/(?P<id>[0-9]+)/$',views.UpdateDoctorProfile.as_view()),
    url(r'speciality-update/(?P<id>[0-9]+)/$',views.UpdateSpecialityApiViews.as_view()),
    url(r'update-procedure/(?P<id>[0-9]+)/$',views.UpdateProcedure.as_view()),
    url(r'update-clinic/(?P<id>[0-9]+)/$',views.UpdateClinic.as_view())
    
]
