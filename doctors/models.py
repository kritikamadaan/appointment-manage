from django.db import models
from django.contrib.postgres.fields import ArrayField
# from users.models import Users
# Create your models here.

class Speciality(models.Model):
    # user = models.ForeignKey(Users, on_delete=models.CASCADE, null=True)
    speciality_name = models.CharField(max_length=300,null=True)
    # procedures = ArrayField(models.CharField(max_length=200), blank=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.speciality_name

class Procedures(models.Model):
    # user = models.ForeignKey(Users, on_delete=models.CASCADE, null=True)
    speciality = models.ForeignKey(Speciality, on_delete=models.CASCADE, null=True)
    procedures = ArrayField(models.CharField(max_length=200), null=True)
    is_active = models.BooleanField(default=True)

class DoctorsProfile(models.Model):
    GENDER_CHOICES = (
        ('male', 'male'),
        ('female', 'female'),
        ('transgender', 'transgender'),
        ('choose not to say', 'choose not to say')
    )
    # user = models.ForeignKey(Users, on_delete=models.CASCADE, null=True)
    first_name = models.CharField(max_length=50,null=True)
    middle_name = models.CharField(max_length=50,null=True, blank=True)
    last_name = models.CharField(max_length=50,null=True)
    gender = models.CharField(max_length=20, choices=GENDER_CHOICES, default='male')
    dob = models.DateField(null=True, blank=True)
    age = models.CharField(max_length=50,null=True,blank=True)
    email = models.CharField(max_length=50,null=True,blank=True)
    mobile_number = models.CharField(max_length=50,null=True,blank=True)
    speciality = models.ForeignKey(Speciality, on_delete=models.CASCADE, null=True,blank=True)
    procedures = ArrayField(models.CharField(max_length=200), blank=True)
    registration_number = models.CharField(max_length=100,null=True,blank=True)
    year_of_registration = models.CharField(max_length=50,null=True,blank=True)
    notes = models.CharField(max_length=1000,null=True, blank=True)
    is_active = models.BooleanField(default=True)
    def __str__(self):
        if self.middle_name:
            return self.first_name+' '+self.middle_name+' '+self.last_name
        return self.first_name+' '+self.last_name

class Clinics(models.Model):
    # user = models.ForeignKey(Users, on_delete=models.CASCADE, null=True)
    clinic_name = models.CharField(max_length=200, null=True)
    clinic_location = models.CharField(max_length=300, null=True)
    longitude = models.CharField(max_length=100, null=True, blank=True)
    latitude = models.CharField(max_length=100, null=True, blank=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.clinic_name
