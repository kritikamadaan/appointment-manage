from django.shortcuts import render

# Create your views here.
class DoctorsProfileListApiViews(APIView):
    permission_classes = (IsSuperUser,)
    def get(self, request):
        userId = request.user.id
        all_doctor = DoctorsProfile.objects.filter(user=userId, is_active=True)
        serializer_class = DoctorsProfileListSerializer(all_doctor, many=True)
        # return Response(serializer_class.data)
        return Response({"status":True,"data":serializer_class.data})



class CreateDoctorProfileApiViews(APIView):
    permission_classes = (IsSuperUser,)
    def post(self,request):
        data = request.data
        data["user"] = request.user.id
        serializer_class = CreateDoctorProfileSerializer(data=data)
        if serializer_class.is_valid():
            serializer_class.save()
            try:
                sp_id = serializer_class.data['speciality']
                speciality = Speciality.objects.get(id=sp_id)
                speciality_serializer = SpecialitySerializer(speciality)
                data = serializer_class.data
                data["speciality"] = speciality_serializer.data
                return Response({"status":True,"data":data})
            except Exception as e:
                pass
        print(serializer_class.errors,">>>>>>>>>>>>>>>")
        return Response({"status":False})



class DoctorsProfileDetailsApiViews(APIView):
    permission_classes = (IsSuperUser,)
    def get(self,request,id):
        userId = request.user.id
        try:
            doctor_details = DoctorsProfile.objects.get(user=userId, id=id,is_active=True)
            serializer_class = DoctorsProfileListSerializer(doctor_details)
            return Response(serializer_class.data)
        except Exception as e:
            return Response({"status":False, "massege": f"detail not found of doctor id: {id}"})

    def delete(self,request,id):
        userId = request.user.id
        try:
            doctor = DoctorsProfile.objects.get(user=userId, id=id, is_active=True)
            doctor.is_active = False
            doctor.save()

            return Response({"status":True, "massage":f"Doctor profile {id} deleted"})
        except Exception as e:
            return Response({"status":False, "massage":f"Doctor profile {id} not deleted"})


class CreateAndListOfClinicsApiViews(APIView):
    permission_classes = (IsSuperUser,)
    def get(self,request):
        userId = request.user.id
        clinics = Clinics.objects.filter(user=userId, is_active=True)
        serializer_class = CreateAndListOfClinicsSerializer(clinics, many=True)
        # return Response(serializer_class.data)
        return Response({"status":True, "data":serializer_class.data})
    
    def post(self,request):
        data = request.data
        data['user'] = request.user.id

        serializer_class = CreateAndListOfClinicsSerializer(data=data)
        if serializer_class.is_valid():
            # print(serializer_class.data)
            serializer_class.save()
            return Response({"status":True, "data":serializer_class.data})
        
        return Response({"status":False,"msg":serializer_class.errors})


class CreateAndListOfProcedures(APIView):
    permission_classes = (IsAdminUser,)
    def get(self,request):
        userId = request.user.id
        procedure_list = Procedures.objects.filter(user=userId,is_active=True)
        serializer_class = ProceduresListSerializer(procedure_list, many=True)
        return Response(serializer_class.data)
        return Response({"status":True,"data":serializer_class.data})

    def post(self,request):
        data = request.data
        data['user'] = request.user.id
        serializer_class = CreateProceduresSerializer(data=data)
        if serializer_class.is_valid():
            # print(serializer_class)
            # print(serializer_class.data)
            serializer_class.save()
            sp_id = serializer_class.data['speciality']
            speciality = Speciality.objects.get(id=sp_id)
            speciality_serializer = SpecialitySerializer(speciality)
            data = serializer_class.data
            data["speciality"] = speciality_serializer.data
            # data={}
            return Response({"status":True,"data":data})
        return Response({"status":False})


class SpecialityDetails(APIView):
    permission_classes = (IsSuperUser,)
    def get(self,request,id):
        userId = request.user.id
        try:
            details = Speciality.objects.get(user=userId, id=id,is_active=True)
            serializer_class = SpecialitySerializer(details)
            return Response({"status":True, "data":serializer_class.data})
        except Exception as e:
            return Response({"status":False, "massege": f"detail not found of speciality id: {id}"})
    
    def delete(self,request,id):
        userId = request.user.id
        try:
            speciality = Speciality.objects.get(user=userId, id=id, is_active=True)
            speciality.is_active = False
            speciality.save()

            return Response({"status":True, "massage":f"Speciality {id} deleted"})
        except Exception as e:
            return Response({"status":False, "massage":f"Speciality {id} not deleted"})



class ProceduresDetails(APIView):
    permission_classes = (IsSuperUser,)
    def get(self,request,id):
        userId = request.user.id
        try:
            details = Procedures.objects.get(user=userId, id=id, is_active=True)
            serializer_class = ProceduresListSerializer(details)
            return Response({"status":True, "data":serializer_class.data})
        except Exception as e:
            return Response({"status":False, "massege": f"detail not found of procedure id: {id}"})

    def delete(self,request,id):
        userId = request.user.id
        try:
            procedure = Procedures.objects.get(user=userId, id=id, is_active=True)
            procedure.is_active = False
            procedure.save()

            return Response({"status":True, "massage":f"Procedure {id} deleted"})
        except Exception as e:
            return Response({"status":False, "massage":f"Procedure {id} not deleted"})



class ClinicDetails(APIView):
    permission_classes = (IsSuperUser,)
    def get(self,request,id):
        userId = request.user.id
        try:
            details = Clinics.objects.get(user=userId, id=id, is_active=True)
            serializer_class = CreateAndListOfClinicsSerializer(details)
            return Response({"status":True, "data":serializer_class.data})
        except Exception as e:
            return Response({"status":False, "massege": f"detail not found of clinic id: {id}"})

    def delete(self,request,id):
        userId = request.user.id
        try:
            clinic = Clinics.objects.get(user=userId, id=id, is_active=True)
            clinic.is_active = False
            clinic.save()

            return Response({"status":True, "massage":f"Clinic {id} deleted"})
        except Exception as e:
            return Response({"status":False, "massage":f"Clinic {id} not deleted"})



class UpdateDoctorProfile(RetrieveUpdateAPIView):
    queryset = DoctorsProfile.objects.all()
    serializer_class = UpdateDoctorSerializer
    lookup_field = 'id'
    permission_classes = (IsSuperUser,)
    def perform_update(self,serializer):
        serializer.save(user = self.request.user)


class UpdateClinic(RetrieveUpdateAPIView):
    queryset = Clinics.objects.all()
    serializer_class = UpdateClinicSerializer
    lookup_field = 'id'
    permission_classes = (IsSuperUser,)
    def perform_update(self,serializer):
        serializer.save(user = self.request.user)



class UpdateProcedure(RetrieveUpdateAPIView):
    queryset = Procedures.objects.all()
    serializer_class = UpdateProcedureSerializer
    lookup_field = 'id'
    permission_classes = (IsSuperUser,)
    def perform_update(self,serializer):
        serializer.save(user = self.request.user


class UpdateSpecialityApiViews(RetrieveUpdateAPIView):
    queryset = Speciality.objects.all()
    serializer_class = UpdateSpecialitySerializer
    lookup_field = 'id'
    permission_classes = (IsSuperUser,)





