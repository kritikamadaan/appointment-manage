from django.urls import path
from django.conf.urls import url
from . import views

# urlpatterns = [
#     path('admin/', admin.site.urls),
#     # path('users/', include('users.urls')),
#     path('doctors/', include('doctors.urls')),
#     path('patients/', include('patients.urls')),
#     path('appointments/', include('appointments.urls')),
#     path("comman/", include("comman.urls"))
# ]


urlpatterns += [
    path('', TemplateView.as_view(template_name='index.html')),
    path('superuser/login', TemplateView.as_view(template_name='index.html')),
    path('dashboard/clinic/list', TemplateView.as_view(template_name='index.html')),
    path('dashboard/appointment/list', TemplateView.as_view(template_name='index.html')),
    path('dashboard/patient/list', TemplateView.as_view(template_name='index.html')),
    path('dashboard/doctor/list', TemplateView.as_view(template_name='index.html')),
    path('dashboard/speciality/list', TemplateView.as_view(template_name='index.html')),

    path("dashboard/procedures/list", TemplateView.as_view(template_name='index.html')),
    path('dashboard/doctor/new', TemplateView.as_view(template_name='index.html')),
    path("dashboard/patient/new", TemplateView.as_view(template_name='index.html')),

    path('appointment/login', TemplateView.as_view(template_name='index.html')),
    path('appointment/register', TemplateView.as_view(template_name='index.html')),
    path('appointment/forgot/password', TemplateView.as_view(template_name='index.html')),

    path('appointment/dashboard', TemplateView.as_view(template_name='index.html')),
    path('appointment/dashboard/doctors', TemplateView.as_view(template_name='index.html')),
    path('appointment/dashboard/add-doctor', TemplateView.as_view(template_name='index.html')),

    path('appointment/dashboard/patients', TemplateView.as_view(template_name='index.html')),
    path('appointment/dashboard/add-patient', TemplateView.as_view(template_name='index.html')),

    path('appointment/dashboard/Speciality', TemplateView.as_view(template_name='index.html')),
    path('appointment/dashboard/add-speciality', TemplateView.as_view(template_name='index.html')),

    path('appointment/dashboard/clinic', TemplateView.as_view(template_name='index.html')),
    path('appointment/dashboard/add-clinic', TemplateView.as_view(template_name='index.html')),

    path('appointment/dashboard/Procedures', TemplateView.as_view(template_name='index.html')),
    path('appointment/dashboard/add-procedures', TemplateView.as_view(template_name='index.html')),

    path('appointment/dashboard/appointments', TemplateView.as_view(template_name='index.html')),
    path('appointment/dashboard/add-appointment', TemplateView.as_view(template_name='index.html')),
    
    url(r'appointment/dashboard/(?P<match>[a-z]+)/profile/(?P<id>[0-9]+)', TemplateView.as_view(template_name='index.html')),
    url(r'appointment/dashboard/edit/(?P<ematch>[a-z]+)/(?P<pId>[0-9]+)', TemplateView.as_view(template_name='index.html'))
]


