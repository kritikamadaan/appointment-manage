from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
# from .models import users

# Register your models here.

class CustomUserAdmin(UserAdmin):
    # model = Users

    list_display = ('email', 'name','is_active', 'is_superuser', 'is_staff')
    # list_filter = ('is_superuser', 'is_active', 'is_staff',)
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Personal info', {'fields': ( 'name',)}),
        ('Permissions', {'fields': ('is_active', 'is_staff', 'is_superuser')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2', 'name', 'is_active', 'is_staff', 'is_superuser',),
        }),
    )
    search_fields = ('email','name')
    ordering = ('email',)
    # filter_horizontal = ()

#admin.site.register(Users, CustomUserAdmin)

