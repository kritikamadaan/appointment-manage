from django.shortcuts import render

from django.shortcuts import render
from django.http import JsonResponse
from django.contrib.auth import authenticate, logout


from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.permissions import AllowAny


from rest_framework.generics import (
    CreateAPIView,
    GenericAPIView, 
    ListCreateAPIView, 
    ListAPIView, 
    RetrieveUpdateAPIView, 
    RetrieveAPIView
)
from .models import Users
from .serializers import UserLoginSerializer, UserDetailsSerializer


from .serializers import UserLoginSerializer, UserDetailsSerializer

from hospital.adminpermissions import IsSuperUser
from patients.models import PatientProfiles
from doctors.models import (
    DoctorsProfile, 
    Speciality,
    Procedures,
    Clinics
)
from appointments.models import PatientAppointments


class UserProfileApiViews(APIView):
    permission_classes = (IsSuperUser,)
    def get(self, request):
        userId = request.user.id
        user_details = Users.objects.get(id=userId)
        serializer_class = UserDetailsSerializer(user_details)

        total_clinic = Clinics.objects.filter(user=userId, is_active=True)
        total_patient = PatientProfiles.objects.filter(user=userId, is_active=True)
        total_speciality = Speciality.objects.filter(user=userId, is_active=True)
        total_doctor = DoctorsProfile.objects.filter(user=userId, is_active=True)
        total_appointment = PatientAppointments.objects.filter(user=userId, is_active=True)
        total_procedures = Procedures.objects.filter(user=userId, is_active=True)

        total = {
            "clinics":len(total_clinic),
            "patients":len(total_patient),
            "speciality":len(total_speciality),
            "appointments":len(total_appointment),
            "doctors":len(total_doctor),
            "procedures":len(total_procedures)
        }
        # print(serializer_class.data,type(serializer_class.data),len(total_appointment))
        return Response({"email":serializer_class.data, "total":total})


class UserLoginApiView(APIView):

    permission_classes = (AllowAny,)
    serializer_class = UserLoginSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
       
        email,user_id = serializer.data['email'].split()
        # print(serializer.data)
        response = {
            'success' : True,
            'message': 'User logged in  successfully',
            'token' : serializer.data['token'],
            'user_id':user_id,
            'email':email
            }
        return Response(response)

class LogoutAPIView(APIView):
    def get(self, request):
        logout(request)
        request.session.flush()
        return Response({"status":True})








