from django.urls import path
from . import views

urlpatterns = [
    path('list/', views.UserProfileApiViews.as_view()),
    path('login/',views.UserLoginApiView.as_view()),
    path('logout/',views.LogoutAPIView.as_view()),
]
