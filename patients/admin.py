from django.contrib import admin


from .models import PatientProfiles, AttendantOfPatient

# Register your models here.
class AdminPatientProfiles(admin.ModelAdmin):
    model = PatientProfiles
    list_display = (
        'first_name',
        'middle_name',
        'last_name',
        'gender',
        'dob',
        # 'clinic',
        'attendant',
        'email',
        'mobile_number',
        'aadhar_no',
        'insurance',
        'is_active'
    )


class AdminAttendantOfPatient(admin.ModelAdmin):
    model = PatientProfiles
    list_display = (
        'attendant_name',
        'attendant_contact',
        'attendant_relationship',
    )


admin.site.register(AttendantOfPatient, AdminAttendantOfPatient)
admin.site.register(PatientProfiles, AdminPatientProfiles)