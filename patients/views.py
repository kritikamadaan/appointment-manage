from rest_framework.permissions import IsAuthenticated, AllowAny, IsAdminUser
from rest_framework.views import APIView
from rest_framework.response import Response
from hospital.adminpermissions import IsSuperUser
from rest_framework.generics import (
    CreateAPIView,
    GenericAPIView, 
    ListCreateAPIView, 
    ListAPIView, 
    RetrieveUpdateAPIView, 
    RetrieveAPIView
)
from .models import PatientProfiles, AttendantOfPatient
from .serializers import (
    PatientsListSerializer,
    CreatePatientsSerializer,
    AttendantOfPatientSerializer,
    ClinicsSerializer,
    UpadatePatientSerializer
)
from doctors.models import Clinics

class PatientsListViews(APIView):
    permission_classes = (IsSuperUser,)
    def get(self, request):
        userId = request.user.id
        patients = PatientProfiles.objects.filter(user=userId, id=id, is_active=True)
        serializer_class = PatientsListSerializer(patients, many=True)
        
        return Response(serializer_class.data)

class CreatePatientApiViews(APIView):
    permission_classes = (IsSuperUser,)
    def post(self,request):
        data = request.data
        attendant = request.data["attendant"]
        data.pop('attendant')
        patient = data
        
        attendant_serializer = AttendantOfPatientSerializer(data=attendant)
        if attendant_serializer.is_valid():
            attendant_serializer.save()
            patient["attendant"] = attendant_serializer.data["id"]
            patient["user"] = request.user.id
            patient_serializer = CreatePatientsSerializer(data=patient)
            if patient_serializer.is_valid():
                patient_serializer.save()

                new_patient = patient_serializer.data
                new_patient["attendant"] = attendant_serializer.data
                try:
                    clinic = Clinics.objects.get(id=new_patient['clinic'])
                    clinic_serializer = ClinicsSerializer(clinic)

                    new_patient['clinic'] = clinic_serializer.data
                except Exception as e:
                    pass
                return Response({"status":True, "data":new_patient})
        # print( patient_serializer.errors)   
        return Response({"status":False})


class PatientDetailsApiViews(APIView):
    permission_classes = (IsSuperUser,)
    def get(self,request,id):
        userId = request.user.id
        try:
            detail = PatientProfiles.objects.get(user=userId, id=id, is_active=True)
            patient_detail = PatientsListSerializer(detail)
            return Response({"status":True, "data":patient_detail.data})
        except Exception as e:
            # print(e,">>>>>>>>>>")
            return Response({"status":False, "massege": f"detail not found of patient id: {id}"})



    def delete(self,request,id):
        userId = request.user.id
        try:
            patient = PatientProfiles.objects.get(user=userId, id=id, is_active=True)
            patient.is_active = False
            patient.save()

            return Response({"status":True, "massage":f"Patient profile {id} deleted"})
        except Exception as e:
            return Response({"status":False, "massage":f"Patient profile not {id} deleted"})



class UpdatePatientApiViews(APIView):
    
    permission_classes = (IsSuperUser,)
    def put(self,request,id):
        userId = request.user.id
        data=request.data
        try:
            attendant = AttendantOfPatient.objects.get(id=data['attendant']['id'])
            attendant.attendant_name = data['attendant']['attendant_name']
            attendant.attendant_contact = data['attendant']['attendant_contact']
            attendant.attendant_relationship = data['attendant']['attendant_relationship']
            attendant.save()

            patient = PatientProfiles.objects.get(user=userId, id=id, is_active=True)
            patient.first_name = data['first_name']
            patient.middle_name = data['middle_name']
            patient.last_name = data['last_name']
            patient.gender = data['gender']
            patient.dob = data['dob']
            patient.age = data['age']
            patient.place_of_birth_city = data['place_of_birth_city']
            patient.place_of_birth_state = data['place_of_birth_state']
            patient.place_of_birth_country = data['place_of_birth_country']
            patient.occupation = data['occupation']
            patient.blood = data['blood']
            patient.clinic = Clinics.objects.get(id=data['clinic'])
            # patient.attendant = data['attendant']
            patient.referred_by = data['referred_by']
            patient.referred_date = data['referred_date']
            patient.email = data['email']
            patient.mobile_number = data['mobile_number']
            patient.aadhar_no = data['aadhar_no']
            # patient.aadhar_image = data['aadhar_image']
            patient.pan_no = data['pan_no']
            # patient.pan_image = data['pan_image']
            patient.insurance = data['insurance']
            patient.notes = data['notes']
            patient.save()

            patient_detail = PatientsListSerializer(patient)
            return Response({"status":True, "data":patient_detail.data})
        except Exception as e:
            return Response({"status":False, "msg":f"Patient details of {id} not updated"})
        
