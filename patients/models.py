from django.db import models
# from doctors.models import Clinics
# from users.models import Users


# Create your models here.
class AttendantOfPatient(models.Model):
    RELATIONSHIP_TYPE = (
        ('father', 'father'),
        ('mother', 'mother'),
        ('spouse', 'spouse'),
        ('son', 'son'),
        ('daughter', 'daughter'),
        ('friend', 'friend')
        # daughter
    )
    attendant_name = models.CharField(max_length=200, null=True, blank=True)
    attendant_contact = models.CharField(max_length=20, null=True, blank=True)
    attendant_relationship = models.CharField(max_length=10, choices=RELATIONSHIP_TYPE, null=True, blank=True)

    def __str__(self):
        return self.attendant_name


class PatientProfiles(models.Model):
    GENDER_CHOICES = (
        ('male', 'male'),
        ('female', 'female'),
        ('transgender', 'transgender'),
        ('choose not to say', 'choose not to say')
    )

    BLOOD_TYPE = (
        ('A+', 'A+'),
        ('A-', 'A-'),
        ('AB+', 'AB+'),
        ('AB-', 'AB-'),
        ('O+', 'O+'),
        ('O-', 'O-'),
        ('B+', 'B+'),
        ('B-', 'B-')
    )
    INSURANCE_TYPE = (
        (True, 'Yes'),
        (False, 'No')
    )
    # user = models.ForeignKey(Users, on_delete=models.CASCADE, null=True)
    first_name  = models.CharField(max_length=50,null=True)
    middle_name = models.CharField(max_length=50,null=True, blank=True)
    last_name = models.CharField(max_length=50,null=True)
    gender = models.CharField(max_length=20, choices=GENDER_CHOICES, default=None)  
    dob = models.DateField(null=True) 
    age = models.CharField(max_length=30,null=True,blank=True)
    place_of_birth_city = models.CharField(max_length=50,null=True, blank=True)
    place_of_birth_state = models.CharField(max_length=50,null=True, blank=True)
    place_of_birth_country = models.CharField(max_length=50,null=True, blank=True)
    occupation = models.CharField(max_length=50,null=True, blank=True)
    blood = models.CharField(max_length=10, choices=BLOOD_TYPE, null=True, blank=True)
    # clinic = models.ForeignKey(Clinics, on_delete=models.CASCADE, null=True, blank=True)
    attendant = models.ForeignKey(AttendantOfPatient, on_delete=models.CASCADE, null=True, blank=True)
    referred_by = models.CharField(max_length=100, null=True, blank=True)
    referred_date = models.DateField(null=True, blank=True) 
    email = models.EmailField(max_length=50,null=True, unique=True,blank=True)
    mobile_number = models.CharField(max_length=20, null=True,blank=True)
    aadhar_no = models.CharField(max_length=20, null=True,blank=True)
    aadhar_image = models.ImageField(null=True, blank=True)
    pan_no = models.CharField(max_length=20, null=True, blank=True)
    pan_image = models.ImageField(null=True, blank=True)
    insurance = models.BooleanField(choices=INSURANCE_TYPE, default=True)
    notes = models.CharField(max_length=1000, null=True, blank=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        if self.middle_name:
            return self.first_name+' '+self.middle_name+' '+self.last_name
        return self.first_name+' '+self.last_name
