from django.urls import path
from django.conf.urls import url
from . import views

urlpatterns = [
    url('list/', views.PatientsListViews.as_view()),
    url('create-profile/',views.CreatePatientApiViews.as_view()),
    url(r'details/(?P<id>[0-9]+)/$', views.PatientDetailsApiViews.as_view()),
    url(r'update/(?P<id>[0-9]+)/$',views.UpdatePatientApiViews.as_view())
]
